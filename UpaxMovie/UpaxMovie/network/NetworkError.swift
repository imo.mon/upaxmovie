//
//  NetworkError.swift
//  UpaxMovie
//
//  Created by Imo on 29/02/24.
//

import Foundation

enum NetworkError: Error {
    case errorUrl
    case errorUnknow
    case errorServer(statusCode: Int)
    case errorDecoding
}

extension NetworkError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .errorUrl:
            return "Error de URL"
        case .errorDecoding:
            return "Error de parseo de datos"
        case .errorUnknow:
            return "Error desconocido"
        case .errorServer(let statusCode):
            return "Error de servicio: \(statusCode)"
        }
    }
}
