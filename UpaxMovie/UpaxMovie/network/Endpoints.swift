//
//  Endpoints.swift
//  UpaxMovie
//
//  Created by Imo on 29/02/24.
//

import Foundation

public protocol EndpointsProtocols {
    var url: String { get }
    var fileName: String { get }
}

struct BaseUrl {
    static let baseURL = "https://api.themoviedb.org/"
}

public enum Endpoints: EndpointsProtocols {
    case movies
    case moviesRecommendations(movieId: Int)
    
    public var url: String {
        switch self {
        case .movies:
            return BaseUrl.baseURL + "3/movie/upcoming"
        case .moviesRecommendations(let movieId):
            return BaseUrl.baseURL + "3/movie/\(movieId)/recommendations"
        }
    }
    
    public var fileName: String {
        switch self {
            case .movies:
                return "Fatal error getting movies"
            case .moviesRecommendations(let movieId):
                return "Fatal error getting movies recommendations"
        }
    }
}
