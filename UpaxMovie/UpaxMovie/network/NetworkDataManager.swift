//
//  NetworkDataManager.swift
//  UpaxMovie
//
//  Created by Imo on 29/02/24.
//

import UIKit

enum HttpMethod: String {
    case get = "GET"
    case post = "POST"
}

protocol NetworkDataProtocol {
    func fetchData<T: Codable>(type: T.Type,
                               method: HttpMethod,
                               url: String,
                               parameters: [String: Any]?,
                               headers: [String: String]?,
                               retries: Int) async throws -> T
}


class NetworkDataManager: NetworkDataProtocol {
    
    private let session: URLSession
    public static let APIKEY = "e26b5978c924226f1ed98dd3ff1288c8"
    
    init(session: URLSession = URLSession.shared) {
        self.session = session
    }
    
    func fetchData<T: Codable>(type: T.Type,
                               method: HttpMethod = .get,
                               url: String,
                               parameters: [String: Any]? = nil,
                               headers: [String: String]? = nil,
                               retries: Int = 3) async throws -> T {
        
        guard let _url = URL(string: url) else {
            throw NetworkError.errorUrl
        }
        
        var urlModified = _url
        if method == .get {
            urlModified = composeGetURL(url: _url, params: parameters)
        }
        
        var request = URLRequest(url: urlModified)
        request.httpMethod = method.rawValue
        
        headers?.forEach { key, value in
            request.addValue(value, forHTTPHeaderField: key)
        }

        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        if method == .post, let parameters = parameters {
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        }
        
        let (data, response) = try await session.data(for: request)
        
        guard let urlResponse = response as? HTTPURLResponse else {
            throw NetworkError.errorUnknow
        }
        
        guard (200..<300).contains(urlResponse.statusCode) else {
           throw NetworkError.errorServer(statusCode: urlResponse.statusCode)
        }
        
        print( "\(url)" )
        let decoder = JSONDecoder()
        do {
            print( String(data: data, encoding: .utf8) ?? "" )
            print( "******************************************************" )
            return try decoder.decode(type.self, from: data)
        } catch {
            print(error)
            print( "******************************************************" )
            throw NetworkError.errorDecoding
        }
    }
    
    //params: Must be [String: String] in order to cast the data Any to String
    func composeGetURL( url: URL, params: [String: Any]? = nil ) -> URL {
        var urlComponets = URLComponents(url: url, resolvingAgainstBaseURL: false)
        
        if let params {
            urlComponets?.queryItems = params.map {
                URLQueryItem(name: $0.key, value: $0.value as? String )
            }
        }
        guard let completePath = urlComponets?.url else { return url }
        return completePath
    }
}
