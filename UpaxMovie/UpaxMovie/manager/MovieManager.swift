//
//  MovieManager.swift
//  UpaxMovie
//
//  Created by Imo on 29/02/24.
//

import Foundation

protocol MovieManagerProtocol {
    func getMovieRecommendations(_ page: Int, movieId: Int, completion: @escaping (MovieResponse?) -> Void)
}

class MovieManager: MovieManagerProtocol {
    let networking = NetworkDataManager()
    
    func getMovieList(_ page: Int, completion: @escaping (MovieResponse?) -> Void) {
        Task {
            do {
                let params = [
                    "page" : String(page),
                    "api_key" : NetworkDataManager.APIKEY
                ]
                let response = try await networking.fetchData(type: MovieResponse?.self,
                                                                      url: Endpoints.movies.url,
                                                                      parameters: params)
                completion(response)
            } catch {
                completion(nil)
            }
        }
    }
    
    func getMovieRecommendations(_ page: Int, movieId: Int, completion: @escaping (MovieResponse?) -> Void) {
        Task {
            do {
                let params = [
                    "page" : String(page),
                    "api_key" : NetworkDataManager.APIKEY
                ]
                let response = try await networking.fetchData(type: MovieResponse?.self,
                                                                      url: Endpoints.moviesRecommendations(movieId: movieId).url,
                                                                      parameters: params)
                completion(response)
            } catch {
                completion(nil)
            }
        }
    }
}
