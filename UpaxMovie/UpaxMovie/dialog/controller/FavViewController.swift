//
//  FavViewController.swift
//  UpaxMovie
//
//  Created Imo on 01/03/24.
//  Template generated by UPAX Zeus
//

import UIKit

class FavViewController: UIViewController {
	var presenter: FavPresenterProtocol?
    
    // MARK: - Private properties
    /// Views for this controller
    public let views = FavViewControllerViews()
    
    var item: Movie?
    
	override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        setupViews()
    }
    
    func setupViews() {
        view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        
        views.titleLB.text = item?.title ?? ""
        views.subtitleLB.text = item?.release_date ?? ""
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(close))
        view.addGestureRecognizer(tap)
    }
    
    @objc func close() {
        presenter?.close()
    }
}

extension FavViewController:FavViewProtocol {
    
}
