//
//  HomeDetailInteractor.swift
//  UpaxMovie
//
//  Created Imo on 29/02/24.
//  Copyright © 2024 ___ORGANIZATIONNAME___. All rights reserved.
//
//  Template generated by UPAX Zeus
//

import UIKit

class HomeDetailInteractor: HomeDetailInteractorInputProtocol {
    weak var presenter: HomeDetailInteractorOutputProtocol?
    
    let movieManager: MovieManager = MovieManager()
    
    func movieRecommendations(_ page: Int, movieId: Int, completion: @escaping (MovieResponse?) -> Void ) {
        movieManager.getMovieRecommendations(page, movieId: movieId, completion: completion)
    }
}
