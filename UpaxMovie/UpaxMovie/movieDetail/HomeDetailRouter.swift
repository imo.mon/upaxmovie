//
//  HomeDetailRouter.swift
//  UpaxMovie
//
//  Created Imo on 29/02/24.
//  Copyright © 2024 ___ORGANIZATIONNAME___. All rights reserved.
//
//  Template generated by UPAX Zeus
//

import UIKit

class HomeDetailRouter: HomeDetailWireframeProtocol {
    weak var viewController: UIViewController?

    static func createModule() -> UIViewController {
        let view = HomeDetailViewController()
        let interactor = HomeDetailInteractor()
        let router = HomeDetailRouter()
        let presenter = HomeDetailPresenter(interface: view, interactor: interactor, router: router)

        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view

        return view
    }
}
