//
//  HomeDatailViewController+DDS.swift
//  UpaxMovie
//
//  Created by Imo on 29/02/24.
//

import UIKit
import Kingfisher

extension HomeDetailViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let item else { return nil }
        let header = HomeDetailHeaderView(reuseIdentifier: HomeDetailHeaderView.identifier)
        header.config(item: item)
        return header
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.results?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let item = data?.results?[indexPath.row],
              let cell = tableView.dequeueReusableCell(withIdentifier: HomeCell.identifier, for: indexPath) as? HomeCell
        else {
            return UITableViewCell()
        }
        
        cell.setSelectedColor()
        cell.titleLB.text = item.title ?? ""
        cell.subtitleLB.text = item.release_date ?? ""
        
        if let url = URL(string: "https://image.tmdb.org/t/p/w500/" + (item.backdrop_path ?? "")) {
            cell.imageMovie.kf.setImage(with: url)
        }
        
        if indexPath.row == (data?.results?.count ?? 0) - 1 {
            loadMore()
        }
        
        return cell
    }
}

extension HomeDetailViewController: UITableViewDelegate {}
