//
//  HomeDetailView.swift
//  UpaxMovie
//
//  Created by Imo on 29/02/24.
//

import UIKit
import Kingfisher

class HomeDetailHeaderView: UITableViewHeaderFooterView {
    
    static let identifier = String(describing: HomeDetailHeaderView.self )
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let mainStack: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .center
        view.spacing = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layoutMargins = UIEdgeInsets(top: 30, left: 20, bottom: 20, right: 20)
        view.isLayoutMarginsRelativeArrangement = true
        return view
    }()
    
    let posterMovie: UIImageView = {
       let imagev = UIImageView()
        imagev.translatesAutoresizingMaskIntoConstraints = false
        imagev.contentMode = .scaleAspectFit
        return imagev
    }()
    
    let titleLB: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = ""
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.numberOfLines = 0
        return label
    }()
    
    let noteLB: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = ""
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 14.0)
        label.numberOfLines = 0
        return label
    }()
    
    let releaseLB: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = ""
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 14.0)
        label.numberOfLines = 0
        return label
    }()
    
    let overviewLB: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = ""
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 12.0)
        label.numberOfLines = 0
        return label
    }()
    
    let relatedLB: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Relacionadas / Recomendadas"
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.numberOfLines = 0
        return label
    }()
    
    func setup() {
        contentView.addSubview(mainStack)
        mainStack.addArrangedSubview(posterMovie)
        mainStack.addArrangedSubview(titleLB)
        mainStack.addArrangedSubview(noteLB)
        mainStack.addArrangedSubview(releaseLB)
        mainStack.addArrangedSubview(overviewLB)
        mainStack.addArrangedSubview(relatedLB)
        
        
        NSLayoutConstraint.activate([
            mainStack.topAnchor.constraint(equalTo: contentView.topAnchor),
            mainStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            mainStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            mainStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            posterMovie.heightAnchor.constraint(equalToConstant: 200),
        ])
    }
    
    func config(item: Movie) {
        guard let url =  URL(string: "https://image.tmdb.org/t/p/w500/" + (item.poster_path ?? ""))  else { return }
        posterMovie.kf.setImage(with: url)
        titleLB.text = item.title
        noteLB.text = "Votos: " + String(format: "%.2f", item.vote_average ?? 0.0)
        releaseLB.text = "Fecha de lanzamiento: \(item.release_date ?? "")"
        overviewLB.text = "Sinopsis:\n\(item.overview ?? "")"
    }
}
