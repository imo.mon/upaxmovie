//
//  HomeCell.swift
//  UpaxMovie
//
//  Created by Imo on 29/02/24.
//

import UIKit

class HomeCell: UITableViewCell {
    
    static let identifier = String(describing: HomeCell.self )
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
 
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let titleLB: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = ""
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.numberOfLines = 0
        return label
    }()
    
    let subtitleLB: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = ""
        label.textColor = .darkGray
        label.font = label.font.withSize(14)
        label.numberOfLines = 0
        return label
    }()
    
    let imageMovie: UIImageView = {
       let imagev = UIImageView()
        imagev.translatesAutoresizingMaskIntoConstraints = false
        return imagev
    }()
    
    func setup() {
        contentView.addSubview(titleLB)
        contentView.addSubview(subtitleLB)
        contentView.addSubview(imageMovie)
        
        NSLayoutConstraint.activate([
            imageMovie.widthAnchor.constraint(equalToConstant: 60),
            imageMovie.heightAnchor.constraint(equalToConstant: 60),
            imageMovie.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            imageMovie.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            
            titleLB.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15),
            titleLB.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 80),
            titleLB.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
            
            subtitleLB.topAnchor.constraint(equalTo: titleLB.bottomAnchor, constant: 10),
            subtitleLB.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 80),
            subtitleLB.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
            subtitleLB.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
            
        ])
    }
    
    func setSelectedColor() {
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.white
        selectedBackgroundView = bgColorView
    }
}
