//
//  HomeInteractor.swift
//  UpaxMovie
//
//  Created Imo on 29/02/24.
//  Copyright © 2024 ___ORGANIZATIONNAME___. All rights reserved.
//
//  Template generated by UPAX Zeus
//

import UIKit

class HomeInteractor: HomeInteractorInputProtocol {
    weak var presenter: HomeInteractorOutputProtocol?
    
    let movieManager: MovieManager = MovieManager()
    
    func movieList(_ page: Int, completion: @escaping (MovieResponse?) -> Void) {
        movieManager.getMovieList(page, completion: completion)
    }
}
