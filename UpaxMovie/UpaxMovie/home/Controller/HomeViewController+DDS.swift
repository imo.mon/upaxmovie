//
//  HomeViewController+DDS.swift
//  UpaxMovie
//
//  Created by Imo on 29/02/24.
//

import UIKit
import Kingfisher

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.results?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let item = data?.results?[indexPath.row],
              let cell = tableView.dequeueReusableCell(withIdentifier: HomeCell.identifier, for: indexPath) as? HomeCell
        else {
            return UITableViewCell()
        }
        
        cell.titleLB.text = item.title ?? ""
        cell.subtitleLB.text = item.release_date ?? ""
        
        if let url = URL(string: "https://image.tmdb.org/t/p/w500/" + (item.backdrop_path ?? "")) {
            cell.imageMovie.kf.setImage(with: url)
        }
        
        if indexPath.row == (data?.results?.count ?? 0) - 1 {
            loadMore()
        }
        
        return cell
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let item = data?.results?[indexPath.row] else { return }
        presenter?.goDetail( item )
    }
}
