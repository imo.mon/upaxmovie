//
//  HomeViewController.swift
//  UpaxMovie
//
//  Created Imo on 29/02/24.
//  Template generated by UPAX Zeus
//

import UIKit

class HomeViewController: UIViewController {
	var presenter: HomePresenterProtocol?
    
    // MARK: - Private properties
    /// Views for this controller
    public let views = HomeViewControllerViews()
    
    var data: MovieResponse?

	override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        configViews()
        callEndpoint()
    }
    
    func configViews() {
        title = "Movie List"
        views.tableView.dataSource = self
        views.tableView.delegate = self
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector( longPress ))
        views.tableView.addGestureRecognizer(longPress)
    }
    
    func callEndpoint() {
        presenter?.movieList(1) { [weak self] data in
            self?.data = data
            DispatchQueue.main.async {
                self?.views.tableView.reloadData()
            }
        }
    }
    
    func loadMore() {
        if (data?.page ?? 1) >= 5 {
            return
        }
        
        data?.page += 1
        presenter?.movieList((data?.page ?? 1)) { [weak self] data in

            var indexPaths: [IndexPath] = []
            let rangeStart = self?.data?.results?.count ?? 0
            let rangeEnd = rangeStart + ((data?.results?.count ?? 0) - 1)
            for i in rangeStart...rangeEnd {
                indexPaths.append(IndexPath(row: i, section: 0))
            }

            self?.data?.results?.append(contentsOf: data?.results ?? [])
            self?.data?.page = data?.page ?? 1
            self?.data?.total_pages = data?.total_pages ?? 1
            
            DispatchQueue.main.async {
                self?.views.tableView.insertRows(at: indexPaths, with: .none)
            }
        }
    }
    
    @objc func longPress(sender: UILongPressGestureRecognizer) {
        if sender.state == UIGestureRecognizer.State.began {
            let touchPoint = sender.location(in: views.tableView)
            if let indexPath = views.tableView.indexPathForRow(at: touchPoint) {
                guard let item = data?.results?[indexPath.row] else { return }
                presenter?.showDialog( item )
            }
        }
    }
}

extension HomeViewController: HomeViewProtocol {
    
}
