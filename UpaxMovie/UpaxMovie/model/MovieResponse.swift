//
//  MovieResponse.swift
//  UpaxMovie
//
//  Created by Imo on 29/02/24.
//

import Foundation

class MovieResponse: Codable {
    var page: Int
    var total_pages: Int
    var results: [Movie]?
}

class Movie: Codable {
    let id: Int
    let adult: Bool?
    let backdrop_path: String?
    let genre_ids: [Int]?
    let original_language: String?
    let original_title: String?
    let overview: String?
    let popularity: Double?
    let poster_path: String?
    let release_date: String?
    let title: String?
    let video: Bool?
    let vote_average: Double?
    let vote_count: Int
}
